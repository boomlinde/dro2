def info(reg):
    if reg == 1:
        return "Test LSI / Enable waveform control"
    if reg == 2:
        return "Timer 1 data"
    if reg == 3:
        return "Timer 2 data"
    if reg == 4:
        return "Timer control flags"
    if reg == 8:
        return "Speech synthesis mode / Keyboard split note select"
    if reg >= 0x20 and reg <= 0x35:
        return "Amp Mod / Vibrato / EG type / Key Scaling / Multiple"
    if reg >= 0x40 and reg <= 0x55:
        return "Key scaling level / Operator output level"
    if reg >= 0x60 and reg <= 0x75:
        return "Attack Rate / Decay Rate"
    if reg >= 0x80 and reg <= 0x95:
        return "Sustain Level / Release Rate"
    if reg >= 0xa0 and reg <= 0xa8:
        return "Frequency (low 8 bits)"
    if reg >= 0xb0 and reg <= 0xb8:
        return "Key On / Octave / Frequency (high 2 bits)"
    if reg == 0xbd:
        return "AM depth / Vibrato depth / Rhythm control"
    if reg >= 0xc0 and reg <= 0xc8:
        return "Feedback strength / Connection type"
    if reg >= 0xe0 and reg <= 0xf5:
        return "Wave Select"
    return 'Unknown'


class Reader():
    def __init__(self, fname):
        self.data = [ord(x) for x in file(fname, 'rb').read()]
        self.index = 0

    def read(self, n):
        out = []
        for i in range(n):
            out.append(self.data[self.index])
            self.index += 1
        if len(out) == 1:
            out = out[0]
        return out

def complex(a):
    a = a[::-1]
    value = 0
    for b in a:
        value <<= 8
        value += b
    return value

def parse(fname):
    reader = Reader(fname)
    r = reader.read

    format = ''.join([chr(x) for x in r(8)])
    print 'File format: %s' % format
    version_major = complex(r(2))
    version_minor = complex(r(2))
    print 'Version: %d.%d' % (version_major, version_minor)

    length_pairs = complex(r(4))
    print '%d pairs' % length_pairs

    length_ms = complex(r(4))
    print '%d ms' % length_ms

    print 'Type %s' % (('OPL2', 'Dual OPL2', 'OPL3')[r(1)])
    print 'Format: %s' % (('Interleaved', '???')[r(1)])
    print 'Compression: %s' % (('None', '???')[r(1)])

    short_delay = r(1)
    long_delay = r(1)
    codemap_length = r(1)
    codemap = r(codemap_length)

    for i in range(length_pairs):
        a = r(1)
        b = r(1)
        if a == short_delay:
            print 'Short delay %d' % (b + 1)
        elif a == long_delay:
            print 'Long delay %d' % (b + 1)
        else:
            reg = codemap[a]
            print '%02x:\t%02x (%s)' % (reg, b, info(reg))

if __name__ == '__main__':
    import sys
    parse(sys.argv[1])

